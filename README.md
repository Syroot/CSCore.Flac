# CSCore.Flac

This library is completely based on the great [CSCore](https://github.com/filoe/cscore) .NET audio library, but only retains the functionality for reading FLAC samples.

As a purely managed FLAC decoder supporting .NET Standard 2.0 was needed, porting the whole CSCore library to .NET Standard 2.0 was neither required nor timely possible, so only the FLAC codec and other dependent classes have been retained.

Alternatively, you can experiment with a (now unsupported) [native libFLAC wrapper](https://gitlab.com/Syroot/FlacNet).

## Usage

You can use the library in exactly the same way as the full CSCore library (except for all the features removed), but since the `CodecFactory` has been removed, you need to instantiate `FlacFile` directly:

```cs
using FlacFile flacFile = new FlacFile(@"yourFile.flac");
using ISampleSource sampleSource = flacFile.ToSampleSource();

float[] samples = new float[4096];
sampleSource.Read(samples, 0, samples.Length);
```

## Removals

In case you want to update the library, the steps to steam down the original files are basically:

- Keep the FLAC codec folder.
- Keep any dependencies it has (especially some of the methods in `Extensions` and `FluentExtensions`).
- Keep the classes required for reading samples (s. source sample above, which must stay compilable).

﻿using System;
using CSCore.Streams.SampleConverter;

namespace CSCore
{
    /// <summary>
    ///     Provides a basic fluent API for creating a source chain.
    /// </summary>
    public static class FluentExtensions
    {
        /// <summary>
        ///     Appends a source to an already existing source.
        /// </summary>
        /// <typeparam name="TInput">Input</typeparam>
        /// <typeparam name="TResult">Output</typeparam>
        /// <param name="input">Already existing source.</param>
        /// <param name="func">Function which appends the new source to the already existing source.</param>
        /// <returns>The return value of the <paramref name="func" /> delegate.</returns>
        public static TResult AppendSource<TInput, TResult>(this TInput input, Func<TInput, TResult> func)
            where TInput : IAudioSource
        {
            return func(input);
        }

        /// <summary>
        ///     Appends a source to an already existing source.
        /// </summary>
        /// <typeparam name="TInput">Input</typeparam>
        /// <typeparam name="TResult">Output</typeparam>
        /// <param name="input">Already existing source.</param>
        /// <param name="func">Function which appends the new source to the already existing source.</param>
        /// <param name="outputSource">Receives the return value.</param>
        /// <returns>The return value of the <paramref name="func" /> delegate.</returns>
        public static TResult AppendSource<TInput, TResult>(this TInput input, Func<TInput, TResult> func,
            out TResult outputSource)
            where TInput : IAudioSource
        {
            outputSource = func(input);
            return outputSource;
        }

        /// <summary>
        ///     Converts a SampleSource to either a Pcm (8, 16, or 24 bit) or IeeeFloat (32 bit) WaveSource.
        /// </summary>
        /// <param name="sampleSource">Sample source to convert to a wave source.</param>
        /// <param name="bits">Bits per sample.</param>
        /// <returns>Wave source</returns>
        public static IWaveSource ToWaveSource(this ISampleSource sampleSource, int bits)
        {
            if (sampleSource == null)
                throw new ArgumentNullException("sampleSource");

            switch (bits)
            {
                case 8:
                    return new SampleToPcm8(sampleSource);
                case 16:
                    return new SampleToPcm16(sampleSource);
                case 24:
                    return new SampleToPcm24(sampleSource);
                case 32:
                    return new SampleToIeeeFloat32(sampleSource);
                default:
                    throw new ArgumentOutOfRangeException("bits", "Must be 8, 16, 24 or 32 bits.");
            }
        }

        /// <summary>
        ///     Converts a <see cref="IWaveSource"/> to IeeeFloat (32bit) <see cref="IWaveSource"/>.
        /// </summary>
        /// <param name="sampleSource">The <see cref="ISampleSource"/> to convert to a <see cref="IWaveSource"/>.</param>
        /// <returns>The <see cref="IWaveSource"/> wrapped around the specified <paramref name="sampleSource"/>.</returns>
        public static IWaveSource ToWaveSource(this ISampleSource sampleSource)
        {
            if (sampleSource == null)
                throw new ArgumentNullException("sampleSource");

            return new SampleToIeeeFloat32(sampleSource);
        }

        /// <summary>
        ///     Converts a <see cref="IWaveSource"/> to a <see cref="ISampleSource"/>.
        /// </summary>
        /// <param name="waveSource">The <see cref="IWaveSource"/> to convert to a <see cref="ISampleSource"/>.</param>
        /// <returns>The <see cref="ISampleSource"/> wrapped around the specified <paramref name="waveSource"/>.</returns>        
        public static ISampleSource ToSampleSource(this IWaveSource waveSource)
        {
            if (waveSource == null)
                throw new ArgumentNullException("waveSource");

            return WaveToSampleBase.CreateConverter(waveSource);
        }
    }
}
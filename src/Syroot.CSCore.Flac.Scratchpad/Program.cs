﻿using System;
using CSCore;
using CSCore.Codecs.FLAC;

namespace Syroot.Flac.Scratchpad
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            using FlacFile flacFile = new FlacFile(@"yourFile.flac");
            using ISampleSource sampleSource = flacFile.ToSampleSource();

            float[] samples = new float[4096];
            sampleSource.Read(samples, 0, samples.Length);

            Console.ReadLine();
        }
    }
}
